// Defining requirements
var gulp = require( 'gulp' );
var uglify = require( 'gulp-uglify' );
var watch = require( 'gulp-watch' );
var sass = require( 'gulp-sass' );
var cssnano = require( 'gulp-cssnano' );
var sourcemaps = require( 'gulp-sourcemaps' );
var autoprefixer = require( 'gulp-autoprefixer' );

gulp.task('sass', async function(){
    return gulp.src('css/*.scss')
        .pipe(sourcemaps.init({loadMaps: true})) // init Source Map
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('css')) // Destination Folder Path
});
gulp.task('watch', async function(){
    gulp.watch('css/*.scss', gulp.series('sass'));
})