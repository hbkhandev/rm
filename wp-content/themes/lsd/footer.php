<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lsd
 */ ?>
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-sm-4 col-lg-2">
                <div class="_logo">
                    <?php if(is_array(get_field('footer_logo','option'))){ ?>
                    <img src="<?php echo get_field('footer_logo','option')['url']; ?>" srcset="<?php echo get_field('footer_logo','option')['url']; ?> 2x" alt="<?php echo get_field('footer_logo','option')['alt']; ?>" />
                    <?php } ?>
                    <div class="_social">
                        <?php if(!empty(get_field('facebook','option'))){ ?>
                        <a href="<?php the_field('facebook','option'); ?>" target="_blank">
                            <i class="fa fa-facebook"></i>
                        </a>
                        <?php } if(!empty(get_field('instagram','option'))){ ?>
                        <a href="<?php the_field('instagram','option'); ?>" target="_blank">
                            <i class="fa fa-instagram"></i>
                        </a>
                        <?php } if(!empty(get_field('twitter','option'))){ ?>
                        <a href="<?php the_field('twitter','option'); ?>" target="_blank">
                            <i class="fa fa-twitter"></i>
                        </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
            <div class="col-6 col-sm-4 col-lg-2">
                <?php dynamic_sidebar( 'f1' ); ?>
            </div>
            <div class="col-6 col-sm-4 col-lg-2">
                <?php dynamic_sidebar( 'f2' ); ?>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="_widget">
                    <h6><?php esc_attr_e('Contact','lsd'); ?></h6>
                    <?php if(!empty(get_field('address_hd','option'))){ ?>
                    <h5 class="mb-4"><?php the_field('address_hd','option'); ?></h5>
                    <?php } if(!empty(get_field('email_hd','option'))){ ?>
                    <p class="mb-4"><a href="mailto:<?php the_field('email_hd','option'); ?>"><?php the_field('email_hd','option'); ?></a> </p>
                    <?php } if(!empty(get_field('phone_hd','option'))){ ?>
                    <p><a href="tel:<?php the_field('phone_hd','option'); ?>"><?php the_field('phone_hd','option'); ?></a> </p>
                    <?php } ?>
                </div>
            </div>
            <div class="col-sm-6 col-lg-3">
                <div class="_map">
                    <?php the_field('google_map_embed_code','option'); ?>
                </div>
            </div>
        </div>
        <?php if(!empty(get_field('copyright_text','option'))){ ?>
        <div class="_copy text-center">
            <p><?php the_field('copyright_text','option'); ?></p>
        </div>
        <?php } ?>
    </div>
</footer>
<?php wp_footer(); ?>
</body>
</html>