<?php
get_header();
while (have_posts()) : the_post(); ?>
<!-- Banner -->
<section class="_abBanner _adj" data-color="#021D49">
    <div class="row no-gutters">
        <div class="col-md-5 my-auto">
            <div class="_txt color-white wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
                <h1 class="_adj"><?php the_title(); ?></h1>
                <?php if(!empty(get_field('primary_text_block_sd'))){ ?>
                <div class="mb-60">
                    <?php the_field('primary_text_block_sd'); ?>
                </div>
                <?php } if(!empty(get_field('continue_reading'))){ ?>
                <a href="#" class="_link _scrollTo"><?php the_field('continue_reading'); ?> <i class="fa fa-long-arrow-down"></i> </a>
                <?php } ?>
            </div>
        </div>
        <?php if(has_post_thumbnail()){ ?>
        <div class="col-md-7">
            <div class="_img wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                <img src="<?php the_post_thumbnail_url(); ?>" srcset="<?php the_post_thumbnail_url(); ?> 2x" alt="<?php the_title(); ?>" class="w-100" />
            </div>
        </div>
        <?php } ?>
    </div>
</section>
<?php the_content();
endwhile;
get_footer(); ?>