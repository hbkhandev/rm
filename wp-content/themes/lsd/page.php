<?php get_header();
while(have_posts()) : the_post(); ?>
<section class="_txtBlock">
    <div class="container">
        <h2 class="mb-4"><?php the_title(); ?></h2>
        <?php the_content(); ?>
    </div>
</section>
<?php endwhile;
get_footer(); ?>