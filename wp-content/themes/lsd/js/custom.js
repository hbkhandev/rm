(function (jQuery) {
    "use strict";
    // Site preloader
    jQuery(window).load(function(){
        jQuery('._loader').fadeOut(
            'slow',
            function(){
                jQuery(this).remove();
            });
    });
    // BG Color & Image
    jQuery('section,div,a,h1,h2,h3,h4,h5,h6,p,span,footer').each(function(){
        var bg_color = jQuery(this).attr("data-color");
        if(bg_color){
            jQuery(this).css("background-color", "" + bg_color + "");
        }
        var txt_color = jQuery(this).attr("data-txt-color");
        if(txt_color){
            jQuery(this).css("color", "" + txt_color + "");
        }
        var l_height = jQuery(this).attr("data-line-height");
        if(l_height){
            jQuery(this).css("line-height", "" + l_height + "");
        }
        var url = jQuery(this).attr("data-image");
        if(url){
            jQuery(this).css("background-image", "url(" + url + ")");
        }
    });
    // Scroll To Bottom
    jQuery('._scrollTo').on('click',function(e){
        e.preventDefault();
        var n_target = jQuery(this).parents('section').next();
        jQuery('html,body').animate({
                scrollTop: jQuery(n_target).offset().top-60},
            'slow');
    });
    // Sticky Navigation
    jQuery(".navbar").sticky({
        topSpacing: 0,
        zIndex: 999
    });
    // WOW
    new WOW().init();
    // PopUp Trigger
    jQuery('._popTrigger').on('click',function (e){
        e.preventDefault();
        if(jQuery(this).hasClass('active')){
            jQuery('#popupModal').fadeOut();
            jQuery(this).removeClass('active');
            jQuery('body').removeClass('_drop');
        } else {
            jQuery('#popupModal').fadeIn();
            jQuery(this).addClass('active');
            jQuery('body').addClass('_drop');
        }
    });
    jQuery('._close').on('click',function (e){
        e.preventDefault();
        jQuery('._popTrigger').removeClass('active');
        jQuery('#popupModal').fadeOut();
        jQuery('body').removeClass('_drop');
    });
    // Sub Menu
    if (jQuery(window).width() < 991) {
        jQuery('.navbar li.menu-item-has-children > a').on('click',function (e){
            e.preventDefault();
            if(jQuery(this).hasClass('active')){
                jQuery(this).removeClass('active');
                jQuery(this).next().slideUp();
            } else {
                jQuery(this).addClass('active');
                jQuery(this).next().slideDown();
            }
        });
    }
})(jQuery);