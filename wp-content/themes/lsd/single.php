<?php get_header();
while(have_posts()) : the_post();
    $facebook_share = 'https://www.facebook.com/sharer/sharer.php?u='.get_the_permalink();
    $twitter_share = 'https://twitter.com/share?text='.get_the_title().'&url='.get_the_permalink();
    $linkedin_share = 'https://www.linkedin.com/sharing/share-offsite/?url='.get_the_permalink(); ?>
    <!-- Banner -->
    <section class="_postBanner p-0">
        <div class="row no-gutters">
            <div class="col-md-6 my-auto">
                <div class="_txt wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
                    <p><?php echo get_the_date(); ?></p>
                    <h2><?php the_title(); ?></h2>
                </div>
            </div>
            <div class="col-md-6">
                <div class="_img wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                    <?php the_post_thumbnail(); ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Text Block -->
    <section class="_txtBlock">
        <div class="container">
            <div class="_max wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                <?php the_content(); ?>
                <div class="_share text-center">
                    <h3>Share Post</h3>
                    <ul>
                        <li><a href="<?php echo $facebook_share; ?>" target="_blank">Facebook</a> </li>
                        <li><a href="<?php echo $linkedin_share; ?>" target="_blank">Linkedin</a> </li>
                        <li><a href="<?php echo $twitter_share; ?>" target="_blank">Twitter</a> </li>
                        <li><a href="mailto:?subject=<?php the_title(); ?>&amp;body=Check out this site <?php the_permalink(); ?>">Email</a> </li>
                    </ul>
                </div>
                <!-- Next/Prev -->
                <div class="_nextPrev d-flex justify-content-between">
                    <?php $nepo = get_next_post();
                    if(!empty($nepo)){
                        $nepoid = $nepo->ID;
                        $next_post_url = get_permalink($nepoid);
                    } else {
                        $next_post_url = "no-posts";
                    } if($next_post_url != "no-posts"){ ?>
                        <a href="<?php echo esc_url($next_post_url); ?>">
                            <i class="fa fa-long-arrow-left"></i>
                            Previous
                        </a>
                    <?php } else { ?>
                        <a href="javascript:void(0);">
                            <i class="fa fa-long-arrow-left"></i>
                            Previous
                        </a>
                    <?php } $prpo = get_previous_post();
                    if(!empty($prpo)){
                        $prpoid = $prpo->ID;
                        $prev_post_url = get_permalink($prpoid);
                    } else {
                        $prev_post_url = "no-post"; }
                    if($prev_post_url != "no-post"){ ?>
                        <a href="<?php echo esc_url($prev_post_url); ?>">
                            Next
                            <i class="fa fa-long-arrow-right"></i>
                        </a>
                    <?php } else{ ?>
                        <a href="javascript:void(0);">
                            Next
                            <i class="fa fa-long-arrow-right"></i>
                        </a>
                    <?php } ?>
                </div>
            </div>
        </div>
    </section>
    <!-- Related Posts -->
    <?php $categories = get_the_category( get_the_ID() );
    $f_cat = $categories[0]->cat_ID;
    $args = array(
        'category__in' => array( $f_cat ),
        'post__not_in' => array( get_the_ID() ),
        'posts_per_page' => 3
    ); ?>
    <section class="_blog pt-0 pb-5 wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
        <div class="container">
            <h2 class="text-center"><?php esc_attr_e('Stay Connected','lsd'); ?></h2>
            <div class="row gutter-60">
                <?php $related_posts = get_posts( $args );
                foreach( $related_posts as $post ): setup_postdata( $post );
                    $txt = get_post_field('post_content', get_the_ID()); ?>
                    <div class="col-md-4">
                        <a href="<?php the_permalink(); ?>" class="_post">
                            <h3><?php the_title(); ?></h3>
                            <h5><?php echo get_the_date(); ?></h5>
                            <p><?php echo lsd_limit_text($txt,30); ?></p>
                        </a>
                    </div>
                <?php endforeach;
                wp_reset_postdata(); ?>
            </div>
        </div>
    </section>
    <?php if(!empty(get_field('heading_bcta','option'))){ ?>
    <!-- CTA -->
    <section class="_cta">
        <div class="row no-gutters">
            <div class="col-md-6 my-auto">
                <div class="_txt wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
                    <h2><?php the_field('heading_bcta','option'); ?></h2>
                    <?php if(!empty(get_field('text_bcta','option'))){ ?>
                    <p><?php the_field('text_bcta','option'); ?></p>
                    <?php } if(!empty(get_field('link_text_bcta','option'))){ ?>
                    <a class="btn" href="<?php the_field('link_url_bcta','option'); ?>"><?php the_field('link_text_bcta','option'); ?> <i class="fa fa-long-arrow-right"></i> </a>
                    <?php } ?>
                </div>
            </div>
            <?php if(is_array(get_field('image_bcta','option'))){ ?>
            <div class="col-md-6">
                <div class="_img wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                    <img src="<?php echo get_field('image_bcta','option')['url']; ?>" srcset="<?php echo get_field('image_bcta','option')['url']; ?> 2x" alt="<?php echo get_field('image_bcta','option')['alt']; ?>" />
                </div>
            </div>
            <?php } ?>
        </div>
    </section>
<?php } endwhile;
get_footer(); ?>