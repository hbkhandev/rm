<?php
if ( ! defined( 'ABSPATH' ) ) {
    header( 'HTTP/1.1 301 Moved Permanently' ); 
    header( 'Location: /' );
    die();
}
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lsd
 */
get_header();
?>
<section>
    <div class="container">
        <? if ( !is_front_page() && !is_home() ): ?>
            <!-- Title Bar HTML -->
        <? endif; ?>
        <? the_content(); ?>
    </div>
</section>
<?php
get_footer();