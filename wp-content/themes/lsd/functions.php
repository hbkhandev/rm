<?php
/**
 * LSD functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package lsd
 */
if ( ! function_exists( 'lsd_setup' ) ) :
	function lsd_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'title-tag' );
		add_theme_support( 'post-thumbnails' );
		register_nav_menus( array(
			'main-menu' => __( 'Main Menu' )
		) );
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );
		add_theme_support( 'custom-background', apply_filters( 'lsd_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );
		add_theme_support( 'customize-selective-refresh-widgets' );
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
        // Set Post Thumbnail Sizes
        if ( function_exists( 'add_image_size' ) ) {
            add_image_size( 'service-thumb', 315, 315, true );
        }
	}
endif;
add_action( 'after_setup_theme', 'lsd_setup' );
/**
 * Enqueue scripts and styles.
 */
require get_template_directory().'/inc/enqueue.php';
/**
 * Register ACF Blocks
 */
require get_template_directory() . '/inc/register-blocks.php';
// Registering Services Post Type
function lsd_services() {
    register_post_type( 'service',
        array(
            'labels' => array(
                'name' => 'Services',
                'singular_name' => 'Service',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Service',
                'edit' => 'Edit',
                'edit_item' => 'Edit Service',
                'new_item' => 'New Service',
                'view' => 'View',
                'view_item' => 'View Service',
                'search_items' => 'Search Service',
                'not_found' => 'No Service found',
                'not_found_in_trash' => 'No Service found in Trash',
                'parent' => 'Parent Service'
            ),
            'public' => true,
            'show_in_rest' => true,
            'supports' => array( 'title','editor','thumbnail'),
            'show_in_nav_menus' => true,
            'has_archive' => true,
            'exclude_from_search' => false,
        )
    );
    flush_rewrite_rules( false );
} add_action( 'init', 'lsd_services' );
// Registering Testimonials Post Type
function lsd_testimonials() {
    register_post_type( 'testimonial',
        array(
            'labels' => array(
                'name' => 'Testimonials',
                'singular_name' => 'Testimonial',
                'add_new' => 'Add New',
                'add_new_item' => 'Add New Testimonial',
                'edit' => 'Edit',
                'edit_item' => 'Edit Testimonial',
                'new_item' => 'New Testimonial',
                'view' => 'View',
                'view_item' => 'View Testimonial',
                'search_items' => 'Search Testimonial',
                'not_found' => 'No Testimonial found',
                'not_found_in_trash' => 'No Testimonial found in Trash',
                'parent' => 'Parent Testimonial'
            ),
            'public' => true,
            'show_in_rest' => false,
            'supports' => array( 'title','editor'),
            'show_in_nav_menus' => false,
            'has_archive' => true,
            'exclude_from_search' => true,
        )
    );
    flush_rewrite_rules( false );
} add_action( 'init', 'lsd_testimonials' );
// Filter the Gravity Forms button type
add_filter("gform_submit_button", "lsd_gf_submit_button", 10, 2);
function lsd_gf_submit_button($button, $form){
    return "<button class='button btn' id='gform_submit_button_{$form["id"]}'><span></span> {$form['button']['text']}</button>";
}
// Limit Text Length
function lsd_limit_text($text, $limit) {
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos   = array_keys($words);
        $text  = substr($text, 0, $pos[$limit]) . '...';
    }
    return wp_strip_all_tags($text);
}
// LSD Pagination
function lsd_pagination($pages = '', $range = 2){
    $showitems = ($range * 2)+1;
    global $paged;
    if(empty($paged)) $paged = 1;
    if($pages == ''){
        global $wp_query;
        $pages = $wp_query->max_num_pages;
        if(!$pages)
        {
            $pages = 1;
        }
    }
    if(1 != $pages){
        echo '<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">';
        if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<li class='page-item'><a class='page-link' href='".get_pagenum_link(1)."'><i class='fa fa-long-arrow-left'></i></a></li>";
        if($paged > 1 && $showitems < $pages) echo "<li class='page-item'><a href='".get_pagenum_link($paged - 1)."'><i class='fa fa-long-arrow-left'></i></a></li>";
        for ($i=1; $i <= $pages; $i++){
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
                echo ($paged == $i)? "<li class='page-item'><a href='javascript:void(0);' class='page-link current'>".$i."</a></li>":"<li class='page-item'><a href='".get_pagenum_link($i)."' class='page-link' >".$i."</a></li>";
            }
        }
        if ($paged < $pages && $showitems < $pages) echo "<li class='page-item'><a class='page-link' href='".get_pagenum_link($paged + 1)."'><i class='fa fa-long-arrow-right'></i></a></li>";
        if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<li class='page-item'><a class='page-link' href='".get_pagenum_link($pages)."'><i class='fa fa-long-arrow-right'></i></a></li>";
        echo "</ul></nav>\n";
    }
}
// Register Options Page
if( function_exists('acf_add_options_page') ) {
    acf_add_options_page(array(
        'page_title' 	=> 'Theme Options',
        'menu_title'	=> 'Theme Options',
        'menu_slug' 	=> 'theme-options-settings',
        'capability'	=> 'edit_posts',
        'redirect'		=> false
    ));

}
add_action( 'init', 'lsd_register_menu' );
// Registering sidebars
function lsd_widgets_init() {
    register_sidebar(array(
        'name' => esc_html__( 'Footer Widget Area 1','lsd' ),
        'id' => 'f1',
        'description' => esc_html__( 'Footer widget area 1','lsd' ),
        'before_title' => '<h6>',
        'after_title' => '</h6>',
        'after_widget' => '</div><div class="clearfix "></div>',
        'before_widget' => '<div id="%1$s" class="_widget %2$s">'
    ));
    register_sidebar(array(
        'name' => esc_html__( 'Footer Widget Area 2','lsd' ),
        'id' => 'f2',
        'description' => esc_html__( 'Footer widget area 2','lsd' ),
        'before_title' => '<h6>',
        'after_title' => '</h6>',
        'after_widget' => '</div><div class="clearfix "></div>',
        'before_widget' => '<div id="%1$s" class="_widget %2$s">'
    ));
}
add_action( 'widgets_init', 'lsd_widgets_init' );
// Menu Classes
function lsd_specific_menu_location_atts( $atts, $item, $args ) {
    if( $args->theme_location == 'main-menu' ) {
        $atts['class'] = 'nav-link';
    }
    return $atts;
}
add_filter( 'nav_menu_link_attributes', 'lsd_specific_menu_location_atts', 10, 3 );