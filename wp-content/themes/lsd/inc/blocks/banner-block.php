<!-- Banner -->
<section class="_banner p-0">
    <div class="container">
        <div class="row flex-column-reverse flex-lg-row">
            <div class="col-lg-5 my-auto">
                <div class="_txt color-white overflow-hidden">
                    <?php if(!empty(get_field('banner_heading_bb'))){ ?>
                        <h1 class="wow animate__animated animate__fadeIn" data-wow-duration="2s"><?php the_field('banner_heading_bb'); ?></h1>
                    <?php } ?>
                    <div class="wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                        <?php the_field('banner_text_bb'); ?>
                    </div>
                    <?php if(!empty(get_field('link_text_bb'))){ ?>
                        <a href="<?php the_field('link_url_bb'); ?>" class="_link wow animate__animated animate__fadeIn" data-wow-duration="2s" data-wow-delay="0.6s"><?php the_field('link_text_bb'); ?> <i class="fa fa-long-arrow-right"></i> </a>
                    <?php } ?>
                </div>
            </div>
            <?php if(is_array(get_field('image_bb'))){ ?>
                <div class="col-lg-6 offset-lg-1">
                    <div class="_img">
                        <img class="wow animate__animated animate__zoomIn" data-wow-duration="1s" data-wow-delay="0.5s" src="<?php echo get_field('image_bb')['url']; ?>" srcset="<?php echo get_field('image_bb')['url']; ?> 2x" alt="<?php echo get_field('image_bb')['alt']; ?>" />
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>
</section>