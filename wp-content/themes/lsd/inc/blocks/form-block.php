<!-- Form -->
<section class="_iForm p-0">
    <div class="row no-gutters">
        <div class="col-md-7 my-auto">
            <div class="_form wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
                <?php if(!empty(get_field('heading_fb'))){ ?>
                <h2><?php the_field('heading_fb'); ?></h2>
                <?php } if(!empty(get_field('gravity_form_id'))){
                    echo do_shortcode('[gravityform id="'.get_field('gravity_form_id').'" title="false" description="false" ajax="true" tabindex="49"]
');
                } ?>
            </div>
        </div>
        <?php if(is_array(get_field('image_fb'))){ ?>
        <div class="col-md-5">
            <div class="_img wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                <img src="<?php echo get_field('image_fb')['url']; ?>" srcset="<?php echo get_field('image_fb')['url']; ?> 2x" alt="<?php echo get_field('image_fb')['alt']; ?>" />
            </div>
        </div>
        <?php } ?>
    </div>
</section>