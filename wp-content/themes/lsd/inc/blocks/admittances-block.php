<section class="_admit">
    <div class="container">
        <?php if(!empty(get_field('heading_ab2'))){ ?>
        <h2 class="text-center wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s"><?php the_field('heading_ab2'); ?></h2>
        <?php } ?>
        <div class="row gutter-60 flex-column-reverse flex-md-row">
            <?php if(is_array(get_field('image_ab2'))){ ?>
            <div class="col-md-5">
                <div class="_img wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                    <img src="<?php echo get_field('image_ab2')['url']; ?>" srcset="<?php echo get_field('image_ab2')['url']; ?> 2x" class="w-100" alt="<?php echo get_field('image_ab2')['alt']; ?>" />
                </div>
            </div>
            <?php } ?>
            <div class="col-md-7">
                <div class="_txt">
                    <ul>
                        <?php $c = 0.2;
                        while(have_rows('add_admittances')) : the_row(); ?>
                        <li class="wow animate__animated animate__fadeIn" data-wow-delay="<?php echo $c; ?>s" data-wow-duration="2s"">
                            <?php if(!empty(get_sub_field('heading'))){ ?>
                            <h3><?php the_sub_field('heading'); ?></h3>
                            <?php } if(!empty(get_sub_field('text'))){ ?>
                            <p><?php the_sub_field('text'); ?></p>
                            <?php } if(!empty(get_sub_field('link_text'))){ ?>
                            <a href="<?php echo get_sub_field('link_url'); ?>" class="_link text-uppercase"><?php the_sub_field('link_text'); ?> <i class="fa fa-long-arrow-right"></i> </a>
                            <?php } ?>
                        </li>
                        <?php $c = $c + 0.2;
                        endwhile; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>