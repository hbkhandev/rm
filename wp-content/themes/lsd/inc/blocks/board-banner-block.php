<!-- Banner -->
<section class="_smallBanner">
    <div class="row no-gutters">
        <div class="col-md-6 my-auto">
            <div class="_txt color-white">
                <?php if(!empty(get_field('heading_bb2'))){ ?>
                    <h2 class="wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s"><?php the_field('heading_bb2'); ?></h2>
                <?php } if(!empty(get_field('text_bb2'))){ ?>
                    <p class="wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s"><?php the_field('text_bb2'); ?></p>
                <?php } ?>
            </div>
        </div>
        <div class="col-md-6">
            <div class="_img text-center">
                <?php if(is_array(get_field('background_image_bb2'))){ ?>
                    <img src="<?php echo get_field('background_image_bb2')['url']; ?>" alt="<?php echo get_field('background_image_bb2')['alt']; ?>" />
                <?php } if(is_array(get_field('logo_bb2'))){ ?>
                    <div class="_logo wow animate__animated animate__fadeIn" data-wow-delay="0.7s" data-wow-duration="2s">
                        <img src="<?php echo get_field('logo_bb2')['url']; ?>" alt="<?php echo get_field('logo_bb2')['alt']; ?>" />
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
</section>