<!-- CTA -->
<section class="_cta">
    <div class="row no-gutters">
        <div class="col-md-6 my-auto">
            <div class="_txt">
                <?php if(!empty(get_field('heading_cta'))){ ?>
                <h2 class="wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s"><?php the_field('heading_cta'); ?></h2>
                <?php } if(!empty(get_field('text_cta'))){ ?>
                <p class="wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s"><?php the_field('text_cta'); ?></p>
                <?php } if(!empty(get_field('link_text_cta'))){ ?>
                <a class="btn wow animate__animated animate__fadeIn" data-wow-delay="0.7s" data-wow-duration="2s" href="<?php the_field('link_url_cta'); ?>"><?php the_field('link_text_cta'); ?> <i class="fa fa-long-arrow-right"></i> </a>
                <?php } ?>
            </div>
        </div>
        <?php if(is_array(get_field('image_cta'))){ ?>
        <div class="col-md-6">
            <div class="_img wow animate__animated animate__fadeIn" data-wow-delay="0.7s" data-wow-duration="2s">
                <img src="<?php echo get_field('image_cta')['url']; ?>" srcset="<?php echo get_field('image_cta')['url']; ?> 2x" alt="<?php echo get_field('image_cta')['alt']; ?>" />
            </div>
        </div>
        <?php } ?>
    </div>
</section>