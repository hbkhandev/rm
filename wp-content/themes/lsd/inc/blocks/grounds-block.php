<!--  Grounds -->
<section class="_grounds">
    <div class="row no-gutters">
        <?php if(is_array(get_field('image_gb'))){ ?>
            <div class="col-md-6">
                <div class="_img wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
                    <img src="<?php echo get_field('image_gb')['url']; ?>" alt="<?php echo get_field('image_gb')['alt']; ?>" />
                </div>
            </div>
        <?php } ?>
        <div class="col-md-6 my-auto">
            <div class="_txt wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                <?php if(!empty(get_field('heading_gb'))){ ?>
                    <h3><?php the_field('heading_gb'); ?></h3>
                <?php } the_field('text_gb'); ?>
            </div>
        </div>
    </div>
</section>