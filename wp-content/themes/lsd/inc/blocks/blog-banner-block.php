<!-- Banner -->
<?php if(!empty(get_field('text_bbb'))){ ?>
<section class="_simpleBanner">
    <div class="container color-white wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
        <?php the_field('text_bbb'); ?>
    </div>
</section>
<?php } ?>