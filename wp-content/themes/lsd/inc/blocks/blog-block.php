<!-- Blog -->
<?php $number = 3;
if(!empty(get_field('number_of_posts'))){
    $number = get_field('number_of_posts');
} global $paged;
if(empty($paged)) $paged = 1;
$args = array(
    'post_type' => 'post',
    'posts_per_page' => $number,
    'paged' => $paged
); $p = new WP_Query($args); ?>
<section class="_blog <?php the_field('disable_top_padding'); ?>">
    <div class="container">
        <?php if(!empty(get_field('heading_bb'))){ ?>
            <h2 class="text-center wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s"><?php the_field('heading_bb'); ?></h2>
        <?php } ?>
        <div class="row gutter-60">
            <?php $c = 0.2;
            while($p->have_posts()) : $p->the_post();
                $txt = get_post_field('post_content', get_the_ID()); ?>
                <div class="col-md-4 wow animate__animated animate__fadeIn" data-wow-delay="<?php echo $c; ?>s" data-wow-duration="2s">
                    <a href="<?php the_permalink(); ?>" class="_post">
                        <h3><?php the_title(); ?></h3>
                        <h5><?php echo get_the_date(); ?></h5>
                        <p><?php echo lsd_limit_text($txt,30); ?></p>
                    </a>
                </div>
            <?php $c = $c + 0.2;
            endwhile;
            wp_reset_postdata(); ?>
        </div>
        <?php if(!empty(get_field('link_text_bb'))){ ?>
            <!-- Link -->
            <div class="text-center wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                <a href="<?php the_field('link_url_bb'); ?>" class="_link text-uppercase"><?php the_field('link_text_bb'); ?> <i class="fa fa-long-arrow-right"></i> </a>
            </div>
        <?php } else{
            lsd_pagination($p->max_num_pages);
        } ?>
    </div>
</section>