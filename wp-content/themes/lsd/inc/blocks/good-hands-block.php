<!-- You Are In Good Hands -->
<section class="_hands">
    <div class="container">
        <div class="row">
            <div class="col-lg-3 my-auto">
                <div class="_txt wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
                    <?php if(!empty(get_field('heading_gh'))){ ?>
                    <h2 class="mb-4"><?php the_field('heading_gh'); ?></h2>
                    <?php } if(!empty(get_field('link_text_gh'))){ ?>
                    <a href="<?php the_field('link_url_gh'); ?>" class="_link text-uppercase"><?php the_field('link_text_gh'); ?> <i class="fa fa-long-arrow-right"></i> </a>
                    <?php } ?>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="row gutter-40">
                    <?php $c = 0.3;
                    while (have_rows('add_values_gh')) : the_row(); ?>
                    <div class="col-md-4 wow animate__animated animate__fadeIn" data-wow-delay="<?php echo $c; ?>s" data-wow-duration="2s">
                        <div class="_value text-center">
                            <?php if(is_array(get_sub_field('icon'))){ ?>
                            <img src="<?php echo get_sub_field('icon')['url']; ?>" alt="<?php echo get_sub_field('icon')['alt']; ?>" />
                            <?php } if(!empty(get_sub_field('heading'))){ ?>
                            <h3><?php the_sub_field('heading'); ?></h3>
                            <?php } if(!empty(get_sub_field('text'))){ ?>
                            <p><?php the_sub_field('text'); ?></p>
                            <?php } ?>
                        </div>
                    </div>
                    <?php $c = $c + 0.3;
                    endwhile; ?>
                </div>
            </div>
        </div>
    </div>
</section>