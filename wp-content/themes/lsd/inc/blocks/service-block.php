<!-- Text  Block -->
<section class="_serviceBlock" data-color="<?php the_field('background_color_sb'); ?>">
    <div class="container">
        <div class="row">
            <?php if(!empty(get_field('heading_sb'))){ ?>
                <div class="col-md-5 offset-lg-1">
                    <div class="_heading wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
                        <h2><?php the_field('heading_sb'); ?></h2>
                    </div>
                </div>
            <?php } ?>
            <div class="col-md-7 col-lg-5 offset-lg-1">
                <div class="_txt wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                    <?php the_field('text_sb'); ?>
                </div>
            </div>
        </div>
    </div>
</section>