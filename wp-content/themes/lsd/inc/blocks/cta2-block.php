<!-- CTA -->
<?php if(!empty(get_field('text_cta2'))){ ?>
<section class="_cta2">
    <div class="container text-center wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
        <p><?php the_field('text_cta2'); ?></p>
    </div>
</section>
<?php } ?>