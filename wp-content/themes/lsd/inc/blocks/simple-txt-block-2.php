<!-- Simple Text Block -->
<section class="_simpleTextBlock <?php the_field('text_color'); ?>" data-color="<?php the_field('background_color_stb2'); ?>">
    <div class="container">
        <div class="_max wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
            <?php the_field('text_stb2'); ?>
        </div>
    </div>
</section>