<!-- Text Block -->
<section class="_txtBlock" data-color="<?php the_field('background_color_stb'); ?>">
    <div class="container">
        <div class="_max wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
            <?php the_field('text_stb'); ?>
        </div>
    </div>
</section>