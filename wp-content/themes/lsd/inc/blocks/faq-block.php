<!--- FAQ -->
<section class="_faq">
    <div class="container">
        <div class="_max750">
            <?php if(!empty(get_field('heading_faq'))){ ?>
            <h2 class="text-center mb-4 wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s"><?php the_field('heading_faq'); ?></h2>
            <?php } if(!empty(get_field('text_faq'))){ ?>
            <p class="mb-5 wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s"><?php the_field('text_faq'); ?></p>
            <?php } ?>
            <div class="accordion _toggle wow animate__animated animate__fadeIn" data-wow-delay="0.7s" data-wow-duration="2s" id="accordionExample">
                <?php $x = 1;
                while(have_rows('add_faqs')) : the_row(); ?>
                <div class="card">
                    <div class="card-header" id="heading<?php echo $x; ?>">
                        <a href="#collapse<?php echo $x; ?>" data-toggle="collapse" data-target="#collapse<?php echo $x; ?>" aria-expanded="<?php if($x == 1){ echo 'true'; } else { echo 'false'; } ?>" aria-controls="collapse<?php echo $x; ?>">
                            <?php the_sub_field('question'); ?>
                        </a>
                    </div>

                    <div id="collapse<?php echo $x; ?>" class="collapse <?php if($x == 1){ echo 'show'; } ?>" aria-labelledby="heading<?php echo $x; ?>" data-parent="#accordionExample">
                        <div class="card-body">
                            <?php the_sub_field('answer'); ?>
                        </div>
                    </div>
                </div>
                <?php $x++;
                endwhile; ?>
            </div>
        </div>
    </div>
</section>