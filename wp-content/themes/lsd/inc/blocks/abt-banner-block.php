<!-- Banner -->
<section class="_abBanner">
    <div class="row no-gutters flex-column-reverse flex-md-row">
        <?php if(!empty(get_field('text_abb'))){ ?>
        <div class="col-md-5 my-auto">
            <div class="_txt wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
                <?php the_field('text_abb'); ?>
            </div>
        </div>
        <?php } if(is_array(get_field('image_abb'))){ ?>
        <div class="col-md-7">
            <div class="_img wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                <img src="<?php echo get_field('image_abb')['url']; ?>" srcset="<?php echo get_field('image_abb')['url']; ?> 2x" alt="<?php echo get_field('image_abb')['alt']; ?>" class="w-100" />
            </div>
        </div>
        <?php } ?>
    </div>
</section>