<!-- Banner -->
<section class="_txtBanner">
    <div class="container color-white">
        <div class="row gutter-60">
            <?php if(!empty(get_field('heading_tbb'))){ ?>
            <div class="col-md-5 offset-md-1">
                <div class="_heading wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
                    <h1><?php the_field('heading_tbb'); ?></h1>
                </div>
            </div>
            <?php } ?>
            <div class="col-md-6  pl-lg-5">
                <div class="_txt pl-md-5 wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                    <?php the_field('text_tbb'); ?>
                </div>
            </div>
        </div>
    </div>
</section>