<!-- Testimonials -->
<section class="_testimonials">
    <div class="container">
        <div class="_top text-center">
            <?php if(!empty(get_field('heading_ts'))){ ?>
                <h2 class="wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s"><?php the_field('heading_ts'); ?></h2>
            <?php } if(!empty(get_field('text_ts'))){ ?>
                <div class="_max750 mb-5 wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
                    <p><?php the_field('text_ts'); ?></p>
                </div>
            <?php } $args = array(
                'post_type' => 'testimonial',
                'posts_per_page' => -1
            ); $t = new WP_Query($args);
            if(get_field('display_type') == 'Carousel'){
                if($t->have_posts()){ ?>
                    <div id="carouselTestimonial" class="carousel slide carousel-fade wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s" data-ride="carousel">
                        <div class="carousel-inner">
                            <?php $x = 1;
                            while($t->have_posts()) : $t->the_post();
                                $txt = get_post_field('post_content', get_the_ID()); ?>
                                <div class="carousel-item <?php if($x == 1) { echo 'active'; } ?>">
                                    <div class="_txt">
                                        <p><?php echo lsd_limit_text($txt,30); ?></p>
                                        <?php if(str_word_count($txt) > 30){ ?>
                                            <a href="#modalID-<?php echo $x; ?>" data-toggle="modal" data-target="#modalID-<?php echo $x; ?>" class="_link text-uppercase"><?php esc_attr_e('Read more','lsd'); ?> <i class="fa fa-plus"></i> </a>
                                        <?php } ?>
                                        <div class="font-weight-bold"><?php the_title(); ?></div>
                                    </div>
                                </div>
                                <?php $x++;
                            endwhile;
                            wp_reset_postdata(); ?>
                        </div>
                        <ol class="carousel-indicators">
                            <?php $y = 0;
                            while($t->have_posts()) : $t->the_post(); ?>
                                <li data-target="#carouselTestimonial" data-slide-to="<?php echo $y; ?>" <?php if($y == 0){ echo 'class="active"'; } ?>></li>
                                <?php $y++;
                            endwhile;
                            wp_reset_postdata(); ?>
                        </ol>
                    </div>
                    <?php $xn = 1;
                    while($t->have_posts()) : $t->the_post();
                        $txt = get_post_field('post_content', get_the_ID());
                        if(str_word_count($txt) > 30){ ?>
                            <!-- Modal -->
                            <div class="modal fade _modal" id="modalID-<?php echo $xn; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <div class="modal-body">
                                            <div class="_test">
                                                <?php the_content(); ?>
                                                <div class="_author"><?php the_title(); ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End Modal -->
                        <?php }
                        $xn++;
                    endwhile;
                    wp_reset_postdata();
                }
            } else { ?>
                <div class="row">
                    <?php $i = 1;
                    $c = 0.1;
                    while($t->have_posts()) : $t->the_post();
                        $txt = get_post_field('post_content', get_the_ID()); ?>
                        <div class="col-md-6 col-lg-4 wow animate__animated animate__fadeIn" data-wow-delay="<?php echo $c; ?>s" data-wow-duration="2s">
                            <div class="_test">
                                <p><?php echo lsd_limit_text($txt,50); ?></p>
                                <?php if(str_word_count($txt) > 50){ ?>
                                    <a href="#modalID-<?php echo $i; ?>" data-toggle="modal" data-target="#modalID-<?php echo $i; ?>" class="_link text-uppercase"><?php esc_attr_e('Read more','lsd'); ?> <i class="fa fa-plus"></i> </a>
                                <?php } ?>
                                <div class="_author"><?php the_title(); ?></div>
                            </div>
                        </div>
                        <?php $c = $c + 0.1; $i++;
                    endwhile;
                    wp_reset_postdata(); ?>
                </div>
                <?php $i2 = 1;
                while($t->have_posts()) : $t->the_post();
                    $txt = get_post_field('post_content', get_the_ID());
                    if(str_word_count($txt) > 50){ ?>
                    <!-- Modal -->
                    <div class="modal fade _modal" id="modalID-<?php echo $i2; ?>" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered">
                            <div class="modal-content">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                                <div class="modal-body">
                                    <div class="_test">
                                        <?php the_content(); ?>
                                        <div class="_author"><?php the_title(); ?></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- End Modal -->
                <?php } $i2++;
                endwhile;
                wp_reset_postdata(); ?>
            <?php } ?>
        </div>
        <?php $logos_ts = get_field('logos_ts');
        if(is_array($logos_ts) && count($logos_ts) > 0){ ?>
            <div class="_bottom d-flex align-items-center">
                <?php $cn = 0.1;
                foreach ($logos_ts as $img){ ?>
                    <div class="wow animate__animated animate__fadeIn" data-wow-delay="<?php echo $cn; ?>s" data-wow-duration="2s">
                        <img src="<?php echo $img['url']; ?>" srcset="<?php echo $img['url']; ?> 2x" alt="<?php echo $img['alt']; ?>" />
                    </div>
                    <?php $cn = $cn + 0.1;
                } ?>
            </div>
        <?php } ?>
    </div>
</section>