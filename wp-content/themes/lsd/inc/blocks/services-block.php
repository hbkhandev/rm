<!-- Area Of Expertise -->
<section class="_area">
    <div class="container text-center wow animate__animated animate__fadeIn" data-wow-delay="0.3s" data-wow-duration="2s">
        <h2><?php the_field('heading_srb'); ?></h2>
        <?php if(get_field('disable_width_limit') == 'No'){ ?><div class="_max"> <?php } ?>
            <p <?php if(get_field('disable_width_limit') == 'Yes'){ echo 'class="mb-5"'; }?>><?php the_field('text_srb'); ?></p>
            <?php if(get_field('disable_width_limit') == 'No'){ ?></div> <?php } ?>
    </div>
    <div class="container-fluid">
        <?php $number = 8;
        if(!empty(get_field('number_of_services'))){
            $number = get_field('number_of_services');
        } $args = array(
            'post_type' => 'service',
            'posts_per_page' => $number
        ); $s = new WP_Query($args);
        if($s->have_posts()){ ?>
        <div class="row gutter-40">
            <?php $c = 0.1;
            while($s->have_posts()) : $s->the_post(); ?>
            <div class="col-sm-6 col-lg-3 wow animate__animated animate__fadeIn" data-wow-delay="<?php echo $c; ?>s" data-wow-duration="2s">
                <a href="<?php the_permalink(); ?>" class="_service overflow-hidden">
                    <span><?php the_title(); ?></span>
                    <?php the_post_thumbnail('service-thumb'); ?>
                </a>
            </div>
            <?php $c = $c + 0.1;
            endwhile;
            wp_reset_postdata(); ?>
        </div>
        <?php } if(!empty(get_field('link_text_srb'))){ ?>
        <!-- Link -->
        <div class="text-center text-uppercase mt-2 wow animate__animated animate__fadeIn" data-wow-delay="0.5s" data-wow-duration="2s">
            <a href="<?php the_field('link_url_srb'); ?>" class="_link"><?php the_field('link_text_srb'); ?> <i class="fa fa-long-arrow-right"></i> </a>
        </div>
        <?php } ?>
    </div>
</section>