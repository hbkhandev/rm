<?php
//First register our custom category
function lsd_gluten_block_category( $categories, $post ) {
	$custom_block = array(
		'slug' => 'lsd-blocks',
		'title' => __( 'LSD Custom Blocks', 'lsd-blocks' )
	);
	$categories_sorted = array();
	$categories_sorted[0] = $custom_block;
	foreach ($categories as $category) {
		$categories_sorted[] = $category;
	}
	return $categories_sorted;
}
add_filter( 'block_categories', 'lsd_gluten_block_category', 10, 2);
//Then register our blocks...
function register_acf_block_types() {
	// Banner Block
	acf_register_block_type(array(
		'name'              => 'banner-block',
		'title'             => __('Banner Block'),
		'description'       => __('You can add home hero to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/banner-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Text Banner Block
	acf_register_block_type(array(
		'name'              => 'txt-banner-block',
		'title'             => __('Text Banner Block'),
		'description'       => __('You can add text banner block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/txt-banner-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// About Banner Block
	acf_register_block_type(array(
		'name'              => 'abt-banner-block',
		'title'             => __('About Banner Block'),
		'description'       => __('You can add about banner block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/abt-banner-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Board Banner Block
	acf_register_block_type(array(
		'name'              => 'board-banner-block',
		'title'             => __('Board Banner Block'),
		'description'       => __('You can add board banner block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/board-banner-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Service Block
	acf_register_block_type(array(
		'name'              => 'service-block',
		'title'             => __('Service Text Block'),
		'description'       => __('You can add service block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/service-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Grounds Block
	acf_register_block_type(array(
		'name'              => 'grounds-block',
		'title'             => __('Grounds Block'),
		'description'       => __('You can add grounds block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/grounds-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Simple Text Block
	acf_register_block_type(array(
		'name'              => 'simple-txt-block',
		'title'             => __('Simple Text Block'),
		'description'       => __('You can add simple text block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/simple-txt-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Simple Text Block 2
	acf_register_block_type(array(
		'name'              => 'simple-txt-block-2',
		'title'             => __('Simple Text Block 2'),
		'description'       => __('You can add simple text block 2 to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/simple-txt-block-2.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// CTA Block 1
	acf_register_block_type(array(
		'name'              => 'cta-block',
		'title'             => __('CTA Block'),
		'description'       => __('You can add cta block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/cta-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// CTA Block 2
	acf_register_block_type(array(
		'name'              => 'cta2-block',
		'title'             => __('CTA Block 2'),
		'description'       => __('You can add cta block 2 to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/cta2-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// FAQ Block
	acf_register_block_type(array(
		'name'              => 'faq-block',
		'title'             => __('FAQ Block'),
		'description'       => __('You can add FAQ block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/faq-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Services Block
	acf_register_block_type(array(
		'name'              => 'services-block',
		'title'             => __('Services Posts Block'),
		'description'       => __('You can add services block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/services-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Form Block
	acf_register_block_type(array(
		'name'              => 'form-block',
		'title'             => __('Form Block'),
		'description'       => __('You can add form block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/form-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Good Hands Block
	acf_register_block_type(array(
		'name'              => 'good-hands-block',
		'title'             => __('Good Hands Block'),
		'description'       => __('You can add good hands block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/good-hands-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Testimonials Block
	acf_register_block_type(array(
		'name'              => 'testimonials-block',
		'title'             => __('Testimonials Block'),
		'description'       => __('You can add testimonials block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/testimonials-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Blog Block
	acf_register_block_type(array(
		'name'              => 'blog-block',
		'title'             => __('Blog Block'),
		'description'       => __('You can add blog block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/blog-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Blog Banner Block
	acf_register_block_type(array(
		'name'              => 'blog-banner-block',
		'title'             => __('Blog Banner Block'),
		'description'       => __('You can add blog banner block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/blog-banner-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
	// Admittances Block
	acf_register_block_type(array(
		'name'              => 'admittances-block',
		'title'             => __('Admittances Block'),
		'description'       => __('You can add admittances block to your content.'),
		'render_template'   => get_template_directory() . '/inc/blocks/admittances-block.php',
		'category'          => 'lsd-blocks',
		'keywords'          => array( '', '' ),
		'mode'              => 'edit',
		'supports'          => array( 'mode' => false )
	));
}
// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
	add_action('acf/init', 'register_acf_block_types');
}