<?php // Load all scripts and stylesheets
function lsd_load_styles() {
	wp_enqueue_style( 'bootstrap' , get_template_directory_uri()."/css/bootstrap.min.css");
	wp_enqueue_style( 'custom' , get_template_directory_uri()."/css/custom.css");
	wp_enqueue_style( 'responsive' , get_template_directory_uri()."/css/responsive.css");
	wp_enqueue_style( 'plugins' , get_template_directory_uri()."/css/plugins.css");
	wp_enqueue_style( 'font-awesome' , get_template_directory_uri()."/fonts/font-awesome/css/font-awesome.min.css");
	wp_enqueue_style( 'style' , get_template_directory_uri()."/style.css");
}
add_action('wp_enqueue_scripts', 'lsd_load_styles');
function lsd_load_scripts() {
	wp_enqueue_script('bootstrap', get_template_directory_uri() . '/js/bootstrap.bundle.min.js', array('jquery'), '', true  );
	wp_enqueue_script('plugins', get_template_directory_uri() . '/js/plugins.js', array('jquery'), '', true  );
	wp_enqueue_script('sticky', get_template_directory_uri() . '/js/jquery.sticky.js', array('jquery'), '', true  );
	wp_enqueue_script('custom', get_template_directory_uri() . '/js/custom.js', array('jquery'), '', true  );
}
add_action('wp_enqueue_scripts', 'lsd_load_scripts');