<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package lsd
 */
get_header(); ?>
    <section>
        <div class="container">
            <h1 class="title"><?php esc_attr_e('PAGE NOT FOUND!','lsd'); ?></h1>
            <p><?php esc_html_e('Looks like the page you’re trying to visit doesn’t exist. Please check the URL and try your luck again.','lsd'); ?></p>
            <a class="btn" href="<?php echo esc_url(home_url('/')); ?>"><?php esc_attr_e('Back to home','lsd'); ?> <i class="fa fa-long-arrow-right"></i> </a>
        </div>
    </section>
<?php
get_footer();