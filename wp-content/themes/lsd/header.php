<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <main>
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lsd
 */ ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> dir="ltr">
<head>
    <meta http-equiv="Content-type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=9;IE=10;IE=Edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1">
    <!-- Fonts -->
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Biryani:wght@300;400;700&family=Nanum+Gothic&family=Roboto:wght@300;400;500;700&display=swap" rel="stylesheet">
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<!-- Loader -->
<div class="_loader">
    <img src="<?php echo get_template_directory_uri(); ?>/images/logo/rachel-moyle-logomark@2x.png" srcset="<?php echo get_template_directory_uri(); ?>/images/logo/rachel-moyle-logomark@2x.png 2x" alt="<?php bloginfo('name'); ?>" />
</div>
<!-- Top Bar -->
<div class="_topBar">
    <div class="container">
        <ul>
            <?php if(!empty(get_field('address_hd','option'))){ ?>
            <li>
                <img src="<?php echo get_template_directory_uri(); ?>/images/map-pin-solid.svg" alt="" />
                <?php the_field('address_hd','option'); ?>
            </li>
            <?php } if(!empty(get_field('working_hours','option'))){ ?>
            <li>
                <img src="<?php echo get_template_directory_uri(); ?>/images/clock-regular.svg" alt="" />
                <?php the_field('working_hours','option'); ?>
            </li>
            <?php } if(!empty(get_field('phone_hd','option'))){ ?>
            <li>
                <img src="<?php echo get_template_directory_uri(); ?>/images/mobile-alt-solid.svg" alt="" />
                <a href="tel:<?php the_field('phone_hd','option'); ?>"><?php the_field('phone_hd','option'); ?></a>
            </li>
            <?php } if(!empty(get_field('email_hd','option'))){ ?>
            <li>
                <img src="<?php echo get_template_directory_uri(); ?>/images/envelope-regular.svg" alt="" />
                <a href="mailto:<?php the_field('email_hd','option'); ?>"><?php the_field('email_hd','option'); ?></a>
            </li>
            <?php } ?>
        </ul>
    </div>
</div>
<nav class="navbar navbar-expand-lg navbar-light">
    <div class="container">
        <?php if(is_array(get_field('header_logo','option'))){ ?>
        <a class="navbar-brand" href="<?php echo esc_url(home_url('/')); ?>">
            <img src="<?php echo get_field('header_logo','option')['url']; ?>" srcset="<?php echo get_field('header_logo','option')['url']; ?> 2x" alt="<?php echo get_field('header_logo')['alt']; ?>" />
        </a>
        <?php } ?>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav ml-auto">
                <?php wp_nav_menu( array( 'theme_location' => 'main-menu','container' => '','depth' => 3, 'items_wrap' => '%3$s') ); ?>
            </ul>
            <?php if(!empty(get_field('header_button_text','option'))){ ?>
            <div class="form-inline mt-2 mt-md-0">
                <a class="btn _popTrigger" href="#popupModal"><?php echo get_field('header_button_text','option'); ?> <i class="fa fa-long-arrow-right"></i> </a>
            </div>
            <?php } ?>
        </div>
    </div>
</nav>
<!-- Popup -->
<div class="p-0 _popUp color-white" id="popupModal" data-image="<?php echo get_template_directory_uri(); ?>/images/blue-bg.png">
    <!-- Close -->
    <a href="#" class="_close">
        <img src="<?php echo get_template_directory_uri(); ?>/images/close-icon/close-icon@2x.png" srcset="<?php echo get_template_directory_uri(); ?>/images/close-icon/close-icon@2x.png 2x" alt="" />
    </a>
    <div class="row no-gutters">
        <?php if(is_array(get_field('popup_image','option'))){ ?>
        <div class="col-xl-4">
            <div class="_img">
                <img class="w-100" src="<?php echo get_field('popup_image','option')['url']; ?>" srcset="<?php echo get_field('popup_image','option')['url']; ?> 2x" alt="<?php echo get_field('popup_image','option')['alt']; ?>" />
            </div>
        </div>
        <?php } ?>
        <div class="col-xl-8 my-auto">
            <div class="row no-gutters">
                <div class="col-md-6">
                    <div class="_txt color-white">
                        <?php if(!empty(get_field('popup_heading','option'))){ ?>
                        <h2><?php the_field('popup_heading','option'); ?></h2>
                        <?php } if(!empty(get_field('popup_text','option'))){ ?>
                        <p><?php the_field('popup_text','option'); ?></p>
                        <?php } ?>
                    </div>
                </div>
                <?php if(!empty(get_field('popup_form_id','option'))){ ?>
                <div class="col-md-6">
                    <div class="___form">
                        <?php echo do_shortcode('[gravityform id="'.get_field('popup_form_id','option').'" title="false" description="false" ajax="true" tabindex="49"]
'); ?>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
</div>